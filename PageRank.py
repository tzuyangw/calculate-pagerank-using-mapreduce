from pyspark import SparkConf, SparkContext

NUM_NODES = 10876
BETA = 0.8
MIN_DELTA = 1e-15
TOP_RANK = 10
MAX_ITERATION = 100

def Out_Link_Mapper(line):
    edges = line.split('\n')
    maplist = []
    for edge in edges:
        nodes = edge.split('\t')
        maplist.append((nodes[0], 1))
    return maplist

def In_Link_Mapper(line):
    edges = line.split('\n')
    maplist = []
    for edge in edges:
        nodes = edge.split('\t')
        maplist.append((nodes[1], 1))
    return maplist

def Edges_Mapper(line):
    edges = line.split('\n')
    maplist = []
    for edge in edges:
        nodes = edge.split('\t')
        maplist.append((nodes[0], nodes[1]))
    return maplist

# The initialization of the pyspark, use Sparkconf() to set up basic configuration of pyspark
# and throw the conf to the SparkContext to declare the pyspark data structure
conf = SparkConf().setMaster("local").setAppName("PageRank")
sc = SparkContext(conf = conf)

# Read the data from .txt file
data = sc.textFile("p2p-Gnutella04.txt")

# Using out_link mapper to map the out link of a node
# and calculate the total number of out link of each node
out_data = data.flatMap(Out_Link_Mapper)
in_data = data.flatMap(In_Link_Mapper)
edge_data = data.flatMap(Edges_Mapper)

# Initialize rj value
ini_data = out_data.union(in_data).reduceByKey(lambda x, y: 1 / NUM_NODES)

# Calculate the number of out link for each node
num_out_link = out_data.reduceByKey(lambda x, y: x + y)

for i in range(MAX_ITERATION):
    # Calculate the out weight for each node
    out_weight = num_out_link.join(ini_data).map(lambda x: (x[0], x[1][1]/x[1][0]))

    # Map the weight that be added in next round
    add_weight = edge_data.join(out_weight).map(lambda x: (x[1][0], x[1][1]))

    # Calculate the new score of nodes without renormalize
    next_data = add_weight.reduceByKey(lambda x, y: x + y).map(lambda x: (x[0], BETA * x[1] + (1 - BETA) / NUM_NODES))

    # Calculate the renormalize value
    temp_score = next_data.map(lambda x: x[1]).collect()
    total_score = 0.0
    renormalize = 0.0
    for score in temp_score:
        total_score += score
        renormalize = 1 - total_score

    # The final next weight of each nodes(preview result + renormalize value)
    next_data = next_data.map(lambda x: (x[0], x[1] + renormalize / NUM_NODES))

    # Judge wether the defference is bigger than min_delta
    change = 0.0
    diff = next_data.union(ini_data).reduceByKey(lambda x, y: abs(x - y)).values()
    for j in diff.collect():
        change += j
    
    if change < MIN_DELTA:
        print("Converge in "+str(i)+" iterations!")
        # print("Iteration "+str(i)+": ")
        # print(next_data.collect())
        print("Defference: "+str(change)+"\n")
        break

    # Renew the next round score data
    ini_data = next_data

    print("Iteration "+str(i)+": ")
    # print(next_data.collect())
    print("Defference: "+str(change)+"\n")

result = next_data.sortBy(lambda x: x[1], ascending=False).collect()

# Write the result to the result.txt
num = 0
with open('result.txt', 'w', encoding = 'UTF-8') as file:
    for rank in result:
        file.write(str(rank[0])+" "+str(rank[1]))
        file.write('\n')
        num += 1
        if num == TOP_RANK:
            break

sc.stop()